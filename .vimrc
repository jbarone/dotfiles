" Modeline and Notes {{{
" vim: set sw=4 ts=4 sts=4 et tw=78 foldmarker={{{,}}} foldlevel=0 foldmethod=marker spell:
"
"   This is the personal vim config of Joshua Barone
"   While much of it is beneficial for general use, I would
"   recommend picking out the parts you want and understand.
"
"   Copyright 2020 Joshua Barone
" }}}

" Environment {{{

    " Basics {{{
        set nocompatible            " Use Vim settings, rather than Vi settings
    " }}}

    " MinPac Setup {{{
        let s:rc_path = expand('~/.vim')

        let s:minpac = expand(s:rc_path . '/pack/minpac/opt/minpac')

        if !isdirectory(glob(s:minpac))
          execute '!git clone https://github.com/k-takata/minpac.git' s:minpac
        endif

        command! PackUpdate packadd minpac | source $MYVIMRC | call minpac#update()
        command! PackClean  packadd minpac | source $MYVIMRC | call minpac#clean()

        packadd minpac
        if exists('*minpac#init')
          " minpac is loaded.
          call minpac#init()
          call minpac#add('k-takata/minpac', {'type': 'opt'})
        endif
    " }}}

" }}}

" General {{{

    " Settings {{{

    filetype plugin indent on           " Automatically detect file types
    syntax on                           " Syntax highlighting

    set shortmess+=filmnrxoOtT          " Abbrev. of messages (avoids 'hit enter')
    set viewoptions=folds,options,cursor,unix,slash " Better Unix / Windows compatibility
    set virtualedit=onemore             " Allow for cursor beyond last character
    set history=1000                    " Store a ton of history (default is 20)
    set spell                           " Spell checking on
    set hidden                          " Allow buffer switching without saving
    set iskeyword-=.                    " '.' is an end of word designator
    set iskeyword-=#                    " '#' is an end of word designator
    set iskeyword-=-                    " '-' is an end of word designator

    set timeout
    set timeoutlen=500
    set encoding=utf8
    scriptencoding utf-8

    set colorcolumn=80,120              " Add vertical lines to columns
    set infercase                       " Case inference search
    set ignorecase                      " Case insensitive search
    set smartcase                       " Case sensitive when uc present
    set magic                           " Turn on magic for regular expressions
    set lazyredraw                      " Don't redraw while executing macros
    set showmatch                       " Show matching pair
    set mat=2

    set noerrorbells
    set novisualbell
    set t_vb=

    set title

    set nrformats=octal,hex

    " set nowrap                          " Do not wrap long lines
    set autoread
    set autoindent                      " Indent at the same level of previous line
    set smartindent                     " Enable smart indentation
    set softtabstop=2                   " Indent by 2 spaces when hitting tab
    set shiftwidth=4                    " Indent by 4 spaces when auto-indenting
    set tabstop=4                       " Show existing tab with 4 spaces width
    set expandtab                       " Tabs are spaces, not tabs
    set nojoinspaces                    " Prevents inserting two spaces after punctuation on a join (J)

    set splitright                      " Puts new vsplit windows to the right
    set splitbelow                      " Puts new split windows below
    set pastetoggle=<F12>

    set laststatus=2                    " show status line
    set statusline=%F%m%r%h%w%=(%{&ff}/%Y)\ (line\ %l\/%L,\ col\ %c)\

    " }}}

    " mouse {{{

    if has('mouse')
        set mouse=a                     " Automatically enable mouse usage
        set mousehide                   " Hide the mouse cursor while typing
    endif

    " }}}

    " clipboard {{{

    if has('clipboard')
        if has('unnamedplus')           " When possible use + register for copy-paste
            set clipboard=unnamed,unnamedplus
        else
            set clipboard=unnamed
        endif
    endif

    " }}}

    " Always switch to the current file directory
    " autocmd BufEnter * if bufname("") !~ "^\[A-Za-z0-9\]*://" | lcd %:p:h | endif

    " backup / undo {{{
        set backup                      " Backups are nice ...
        if has('persistent_undo')
            set undofile                " So is persistent undo ...
            set undolevels=1000         " Maximum number of changes that can be undone
            set undoreload=10000        " Maximum number of lines to save for undo on a buffer reload
        endif
    " }}}

" }}}

" Plugins {{{

    " General {{{
        call minpac#add('godlygeek/tabular')
        call minpac#add('ctrlpvim/ctrlp.vim')
        call minpac#add('tacahiroy/ctrlp-funky')
        call minpac#add('scrooloose/nerdtree')
        call minpac#add('jistr/vim-nerdtree-tabs')
        call minpac#add('altercation/vim-colors-solarized')
        call minpac#add('tpope/vim-surround')
        call minpac#add('tpope/vim-repeat')
        call minpac#add('rhysd/conflict-marker.vim')
        call minpac#add('jiangmiao/auto-pairs')
        call minpac#add('vim-scripts/matchit.zip')
        call minpac#add('vim-airline/vim-airline')
        call minpac#add('vim-airline/vim-airline-themes')
        call minpac#add('powerline/fonts')
        call minpac#add('nathanaelkane/vim-indent-guides')
        call minpac#add('mhinz/vim-signify')
        call minpac#add('osyo-manga/vim-over')
        call minpac#add('kana/vim-textobj-user')
        call minpac#add('kana/vim-textobj-indent')
        call minpac#add('gcmt/wildfire.vim')
        call minpac#add('tpope/vim-unimpaired')
        call minpac#add('rbgrouleff/bclose.vim')
    " }}}

    " Writing {{{
        call minpac#add('reedes/vim-litecorrect')
        call minpac#add('reedes/vim-textobj-sentence')
        call minpac#add('reedes/vim-textobj-quote')
        call minpac#add('reedes/vim-wordy')
    " }}}

    " General Programming {{{
        call minpac#add('w0rp/ale')
        call minpac#add('tpope/vim-fugitive')
        call minpac#add('scrooloose/nerdcommenter')
        call minpac#add('tpope/vim-commentary')
        call minpac#add('luochen1990/rainbow')
        if executable('ctags')
            call minpac#add('majutsushi/tagbar')
        endif
        call minpac#add('airblade/vim-gitgutter')
        call minpac#add('Xuyuanp/nerdtree-git-plugin')
        call minpac#add('gregsexton/gitv')
    " }}}

    " Go Lang {{{
        call minpac#add('fatih/vim-go')
        call minpac#add('buoto/gotests-vim')
        call minpac#add('godoctor/godoctor.vim')
    " }}}

    " HTML {{{
        call minpac#add('vim-scripts/HTML-AutoCloseTag')
        call minpac#add('hail2u/vim-css3-syntax')
        call minpac#add('gorodinskiy/vim-coloresque')
    " }}}

    " Python {{{
        call minpac#add('klen/python-mode')
        call minpac#add('yssource/python.vim')
        call minpac#add('vim-scripts/python_match.vim')
    " }}}

    " TMUX {{{
        call minpac#add('christoomey/vim-tmux-navigator')
        call minpac#add('christoomey/vim-tmux-runner')
        call minpac#add('keith/tmux.vim')
    " }}}

" }}}

" UI {{{

    " Automatically rebalance windows on vim resize
    autocmd VimResized * :wincmd =

    " zoom a vim pane, <C-w>= to rebalance
    nnoremap <leader>- :wincmd _<CR>:wincmd \|<CR>
    nnoremap <leader>= :wincmd =<CR>

    " background {{{

        set background=dark                 " Assume a dark background

        " Allow to trigger background toggle between light and dark
        function! ToggleBG()
            let s:tbg = &background
            " Inversion
            if s:tbg == "dark"
                set background=light
            else
                set background=dark
            endif
        endfunction

        noremap <leader>bg :call ToggleBG()<CR>

    " }}}

    let g:solarized_termcolors=256
    let g:solarized_termtrans=1
    let g:solarized_contrast="normal"
    let g:solarized_visibility="normal"
    try
        color solarized                     " Load a color scheme
    catch
        color desert
    endtry

    set tabpagemax=15                   " Only show 15 tabs
    set showmode                        " Display the current mode

    set cursorline                      " Highlight current line

    highlight clear SignColumn          " SignColumn should match background
    highlight clear LineNr              " Current line number row will have same
                                        " background color in relative mode
    "highlight clear CursorLineNr       " Remove highlight color from current line number

    if has('cmdline_info')
        set ruler                       " Show the ruler
        set rulerformat=%30(%=\:b%n%y%m%r%w\ %l,%c%V\ %P%) " A ruler on steroids
        set showcmd                     " Show partial commands in status line and
                                        " Selected characters/lines in visual mode
    endif

    set backspace=indent,eol,start      " Backspace for dummies
    set linespace=0                     " No extra spaces between rows
    set number                          " Line numbers on
    set relativenumber
    set showmatch                       " Show matching brackets/parenthesis
    set incsearch                       " Find as you type search
    set hlsearch                        " Highlight search terms
    set winminheight=0                  " Windows can be 0 line high
    set wildmenu                        " Show list instead of just completing
    set wildmode=list:longest,full      " Command <Tab> completion, list matches, then longest common part, then all.
    set whichwrap=b,s,h,l,<,>,[,]       " Backspace and cursor keys wrap too
    set scrolljump=5                    " Lines to scroll when cursor leaves screen
    set scrolloff=3                     " Minimum lines to keep above and below cursor
    set foldenable                      " Auto fold code
    set list
    set listchars=tab:›\ ,trail:•,extends:#,nbsp:. " Highlight problematic whitespace

" }}}

" Functions {{{

    " Initialize directories {{{
    function! InitializeDirectories()
        let parent = $HOME
        let prefix = 'vim'
        let dir_list = {
                    \ 'undo': 'undodir',
                    \ 'backup': 'backupdir',
                    \ 'views': 'viewdir',
                    \ 'swap': 'directory' }

        if has('persistent_undo')
            let dir_list['undo'] = 'undodir'
        endif

        let common_dir = parent . '/.' . prefix

        for [dirname, settingname] in items(dir_list)
            let directory = common_dir . dirname . '/'
            if exists("*mkdir")
                if !isdirectory(directory)
                    call mkdir(directory)
                endif
            endif
            if !isdirectory(directory)
                echo "Warning: Unable to create backup directory: " . directory
                echo "Try: mkdir -p " . directory
            else
                let directory = substitute(directory, " ", "\\\\ ", "g")
                exec "set " . settingname . "=" . directory
            endif
        endfor
    endfunction
    call InitializeDirectories()
    " }}}

    " Initialize NERDTree as needed {{{
    function! NERDTreeInitAsNeeded()
        redir => bufoutput
        buffers!
        redir END
        let idx = stridx(bufoutput, "NERD_tree")
        if idx > -1
            NERDTreeMirror
            NERDTreeFind
            wincmd l
        endif
    endfunction
    " }}}

    " Strip whitespace {{{
    function! StripTrailingWhitespace()
        " Preparation: save last search, and cursor position.
        let _s=@/
        let l = line(".")
        let c = col(".")
        " do the business:
        %s/\s\+$//e
        " clean up: restore previous search history, and cursor position
        let @/=_s
        call cursor(l, c)
    endfunction
    " }}}

" }}}

" Formatting {{{

    autocmd BufNewFile,BufRead *.coffee set filetype=coffee
    autocmd BufNewFile,BufRead *.html.twig set filetype=html.twig
    autocmd BufNewFile,BufRead *.jsx set filetype=javascript

    autocmd FileType c,cpp,java,go,php,javascript,puppet,python,rust,twig,xml,yml,perl,sql,coffee,ruby autocmd BufWritePre <buffer> call StripTrailingWhitespace()
    autocmd FileType haskell,puppet,ruby,yml,javascript setlocal expandtab shiftwidth=2 softtabstop=2

    " Workaround vim-commentary for Haskell
    autocmd FileType haskell setlocal commentstring=--\ %s
    " Workaround broken colour highlighting in Haskell
    autocmd FileType haskell,rust setlocal nospell

" }}}

" Key (re)Mappings {{{

    " Map escape for insert mode
    imap jk <Esc>
    imap kj <Esc>

    " Move between buffers with Shift + arrow key
    nnoremap <S-Left> :bprevious<cr>
    nnoremap <S-Right> :bnext<cr>

    " ... but skip the quickfix when navigating
    augroup qf
        autocmd!
        autocmd FileType qf set nobuflisted
    augroup END

    let mapleader = ','
    let maplocalleader = '_'

    " Wrapped lines goes down/up to next row, rather than next line in file.
    noremap j gj
    noremap k gk

    " Yank from the cursor to the end of the line, to be consistent with C and D.
    nnoremap Y y$

    nmap <silent> <leader>/ :set invhlsearch<CR>

    " Find merge conflict markers
    map <leader>fc /\v^[<\|=>]{7}( .*\|$)<CR>

    " Shortcuts
    " Change Working Directory to that of the current file
    cmap cwd lcd %:p:h
    cmap cd. lcd %:p:h

    " Visual shifting (does not exit Visual mode)
    vnoremap < <gv
    vnoremap > >gv

    " Allow using the repeat operator with a visual selection (!)
    " http://stackoverflow.com/a/8064607/127816
    vnoremap . :normal .<CR>

    " For when you forget to sudo.. Really Write the file.
    cmap w!! w !sudo tee % >/dev/null

    " Map <Leader>ff to display all lines with keyword under cursor
    " and ask which one to jump to
    nmap <Leader>ff [I:let nr = input("Which one: ")<Bar>exe "normal " . nr ."[\t"<CR>

    " Easier horizontal scrolling
    map zl zL
    map zh zH

    " Easier formatting
    nnoremap <silent> <leader>q gwip

    noremap <leader><Space> :call StripTrailingWhitespace()<CR>

    " Stupid shift key fixes
    if has("user_commands")
        command! -bang -nargs=* -complete=file W w<bang> <args>
    endif

" }}}

" GUI Settings {{{

    " GVIM- (here instead of .gvimrc)
    if has('gui_running')
        set guioptions-=T           " Remove the toolbar
        set lines=40                " 40 lines of text instead of 24
        if LINUX() && has("gui_running")
            set guifont=Hack\ Regular\ 12,Andale\ Mono\ Regular\ 12,Menlo\ Regular\ 11,Consolas\ Regular\ 12,Courier\ New\ Regular\ 14
        elseif OSX() && has("gui_running")
            set guifont=Hack\ Regular:h12,Andale\ Mono\ Regular:h12,Menlo\ Regular:h11,Consolas\ Regular:h12,Courier\ New\ Regular:h14
        elseif WINDOWS() && has("gui_running")
            set guifont=Hack:h10,Andale_Mono:h10,Menlo:h10,Consolas:h10,Courier_New:h10
        endif
    else
        if &term == 'xterm' || &term == 'screen'
            set t_Co=256            " Enable 256 colors to stop the CSApprox warning and make xterm vim shine
        endif
        "set term=builtin_ansi       " Make arrow and other keys work
    endif

" }}}

" Plugin Configs {{{

    " Ale {{{
        let g:ale_sign_error = '✘'
        let g:ale_sign_warning = '⚠'
        highlight ALEErrorSign ctermbg=NONE ctermfg=red
        highlight ALEWarningSign ctermbg=NONE ctermfg=yellow
        let g:ale_linters = {'javascript': ['eslint']}
    " }}}

    " GoLang {{{
        let g:go_highlight_types = 1
        let g:go_highlight_fields = 1
        let g:go_highlight_functions = 1
        let g:go_highlight_methods = 1
        let g:go_highlight_structs = 1
        let g:go_highlight_operators = 1
        let g:go_highlight_build_constraints = 1
        let g:go_highlight_extra_types = 1
        let g:go_fmt_command = "goimports"
        let g:go_snippet_engine = "neosnippet"
        " Show the progress when running :GoCoverage
        let g:go_echo_command_info = 1

        " Show type information
        let g:go_auto_type_info = 1

        " Highlight variable uses
        let g:go_auto_sameids = 1

        " Fix for location list when vim-go is used together with Syntastic
        let g:go_list_type = "quickfix"

        " Add the failing test name to the output of :GoTest
        let g:go_test_show_name = 1

        " gometalinter configuration
        let g:go_metalinter_command = ""
        let g:go_metalinter_deadline = "5s"
        let g:go_metalinter_enabled = [
            \ 'deadcode',
            \ 'gas',
            \ 'goconst',
            \ 'gocyclo',
            \ 'golint',
            \ 'gosimple',
            \ 'ineffassign',
            \ 'vet',
            \ 'vetshadow'
        \]

        " Set whether the JSON tags should be snakecase or camelcase.
        let g:go_addtags_transform = "camelcase"

        au FileType go set noexpandtab
        au FileType go set shiftwidth=4
        au FileType go set softtabstop=4
        au FileType go set tabstop=4
        au FileType go nmap <Leader>s <Plug>(go-implements)
        au FileType go nmap <Leader>i <Plug>(go-info)
        au FileType go nmap <Leader>e <Plug>(go-rename)
        au FileType go nmap <leader>r <Plug>(go-run)
        au FileType go nmap <leader>b <Plug>(go-build)
        au FileType go nmap <leader>t <Plug>(go-test)
        au FileType go nmap <Leader>gd <Plug>(go-doc)
        au FileType go nmap <Leader>gv <Plug>(go-doc-vertical)
        au FileType go nmap <leader>co <Plug>(go-coverage)
        au FileType go nmap <leader>ct <Plug>(go-coverage-toggle)
        au FileType go nmap <Leader>gb <Plug>(go-doc-browser)
        autocmd FileType go command! -bang A call go#alternate#Switch(<bang>0, 'edit')
        autocmd FileType go command! -bang AS call go#alternate#Switch(<bang>0, 'split')
        autocmd FileType go command! -bang AV call go#alternate#Switch(<bang>0, 'vsplit')
        autocmd FileType go command! -bang AT call go#alternate#Switch(<bang>0, 'tabe')

        let g:delve_backend = "native"
    " }}}

    " TextObj Sentence {{{
        augroup textobj_sentence
          autocmd!
          autocmd FileType markdown call textobj#sentence#init()
          autocmd FileType textile call textobj#sentence#init()
          autocmd FileType text call textobj#sentence#init()
        augroup END
    " }}}

    " TextObj Quote {{{
        augroup textobj_quote
            autocmd!
            autocmd FileType markdown call textobj#quote#init()
            autocmd FileType textile call textobj#quote#init()
            autocmd FileType text call textobj#quote#init({'educate': 0})
        augroup END
    " }}}

    " Misc {{{
        if isdirectory(expand(s:rc_path . "/pack/minpac/start/nerdtree"))
            let g:NERDShutUp=1
        endif
        if isdirectory(expand(s:rc_path . "/pack/minpac/start/matchit.zip"))
            let b:match_ignorecase = 1
        endif
    " }}}

    " Ctags {{{
        set tags=./tags;/,~/.vimtags

        " Make tags placed in .git/tags file available in all levels of a repository
        let gitroot = substitute(system('git rev-parse --show-toplevel'), '[\n\r]', '', 'g')
        if gitroot != ''
            let &tags = &tags . ',' . gitroot . '/.git/tags'
        endif
    " }}}

    " AutoCloseTag {{{
        " Make it so AutoCloseTag works for xml and xhtml files as well
        au FileType xhtml,xml ru ftplugin/html_autoclosetag.vim
    " }}}

    " NerdTree {{{
        if isdirectory(expand(s:rc_path . "/pack/minpac/start/nerdtree"))
            map <C-e> <plug>NERDTreeTabsToggle<CR>
            map <leader>e :NERDTreeFind<CR>
            nmap <leader>nt :NERDTreeFind<CR>

            let NERDTreeShowBookmarks=1
            let NERDTreeIgnore=['\.py[cd]$', '\~$', '\.swo$', '\.swp$', '^\.git$', '^\.hg$', '^\.svn$', '\.bzr$']
            let NERDTreeChDirMode=0
            let NERDTreeQuitOnOpen=1
            let NERDTreeMouseMode=2
            let NERDTreeShowHidden=1
            let NERDTreeKeepTreeInNewTab=1
            let g:nerdtree_tabs_open_on_gui_startup=0
        endif
    " }}}

    " Tabularize {{{
        if isdirectory(expand(s:rc_path . "/pack/minpac/start/tabular"))
            nmap <Leader>a& :Tabularize /&<CR>
            vmap <Leader>a& :Tabularize /&<CR>
            nmap <Leader>a= :Tabularize /^[^=]*\zs=<CR>
            vmap <Leader>a= :Tabularize /^[^=]*\zs=<CR>
            nmap <Leader>a=> :Tabularize /=><CR>
            vmap <Leader>a=> :Tabularize /=><CR>
            nmap <Leader>a: :Tabularize /:<CR>
            vmap <Leader>a: :Tabularize /:<CR>
            nmap <Leader>a:: :Tabularize /:\zs<CR>
            vmap <Leader>a:: :Tabularize /:\zs<CR>
            nmap <Leader>a, :Tabularize /,<CR>
            vmap <Leader>a, :Tabularize /,<CR>
            nmap <Leader>a,, :Tabularize /,\zs<CR>
            vmap <Leader>a,, :Tabularize /,\zs<CR>
            nmap <Leader>a<Bar> :Tabularize /<Bar><CR>
            vmap <Leader>a<Bar> :Tabularize /<Bar><CR>
        endif
    " }}}

    " JSON {{{
        nmap <leader>jt <Esc>:%!python -m json.tool<CR><Esc>:set filetype=json<CR>
        let g:vim_json_syntax_conceal = 0
    " }}}

    " Javascript {{{
        let g:jsx_ext_required = 0 " Allow JSX in normal JS files"
    " }}}

    " PyMode {{{
        " Disable if python support not present
        if !has('python') && !has('python3')
            let g:pymode = 0
        endif

        if isdirectory(expand(s:rc_path . "/pack/minpac/start/python-mode"))
            let g:pymode_lint_checkers = ['pyflakes']
            let g:pymode_trim_whitespaces = 0
            let g:pymode_options = 0
            let g:pymode_rope = 0
        endif
    " }}}

    " ctrlp {{{
        if isdirectory(expand(s:rc_path . "/pack/minpac/start/ctrlp.vim/"))
            let g:ctrlp_working_path_mode = 'ra'
            nnoremap <silent> <D-t> :CtrlP<CR>
            nnoremap <silent> <D-r> :CtrlPMRU<CR>
            let g:ctrlp_custom_ignore = {
                \ 'dir':  '\.git$\|\.hg$\|\.svn$',
                \ 'file': '\.exe$\|\.so$\|\.dll$\|\.pyc$' }

            if executable('ag')
                let s:ctrlp_fallback = 'ag %s --nocolor -l -g ""'
            elseif executable('ack-grep')
                let s:ctrlp_fallback = 'ack-grep %s --nocolor -f'
            elseif executable('ack')
                let s:ctrlp_fallback = 'ack %s --nocolor -f'
            else
                let s:ctrlp_fallback = 'find %s -type f'
            endif
            if exists("g:ctrlp_user_command")
                unlet g:ctrlp_user_command
            endif
            let g:ctrlp_user_command = {
                \ 'types': {
                    \ 1: ['.git', 'cd %s && git ls-files . --cached --exclude-standard --others'],
                    \ 2: ['.hg', 'hg --cwd %s locate -I .'],
                \ },
                \ 'fallback': s:ctrlp_fallback
            \ }

            if isdirectory(expand("~/.vim/pack/minpac/start/ctrlp-funky/"))
                " CtrlP extensions
                let g:ctrlp_extensions = ['funky']

                "funky
                nnoremap <Leader>fu :CtrlPFunky<Cr>
            endif
        endif
    "}}}

    " TagBar {{{
        autocmd VimEnter * if exists(":TagbarToggle") | exe "nnoremap <silent> <leader>tt :TagbarToggle<CR>" | endif
        let g:tagbar_type_go = {
            \ 'ctagstype' : 'go',
            \ 'kinds'     : [
                \ 'p:package',
                \ 'i:imports:1',
                \ 'c:constants',
                \ 'v:variables',
                \ 't:types',
                \ 'n:interfaces',
                \ 'w:fields',
                \ 'e:embedded',
                \ 'm:methods',
                \ 'r:constructor',
                \ 'f:functions'
            \ ],
            \ 'sro' : '.',
            \ 'kind2scope' : {
                \ 't' : 'ctype',
                \ 'n' : 'ntype'
            \ },
            \ 'scope2kind' : {
                \ 'ctype' : 't',
                \ 'ntype' : 'n'
            \ },
            \ 'ctagsbin'  : 'gotags',
            \ 'ctagsargs' : '-sort -silent'
        \ }
    "}}}

    " Rainbow {{{
        if isdirectory(expand(s:rc_path . "/pack/minpac/start/rainbow/"))
            let g:rainbow_active = 1 "0 if you want to enable it later via :RainbowToggle
        endif
    " }}}

    " Fugitive {{{
        if isdirectory(expand(s:rc_path . "/pack/minpac/start/vim-fugitive/"))
            nnoremap <silent> <leader>gs :Gstatus<CR>
            nnoremap <silent> <leader>gd :Gdiff<CR>
            nnoremap <silent> <leader>gc :Gcommit<CR>
            nnoremap <silent> <leader>gb :Gblame<CR>
            nnoremap <silent> <leader>gl :Glog<CR>
            nnoremap <silent> <leader>gp :Git push<CR>
            nnoremap <silent> <leader>gr :Gread<CR>
            nnoremap <silent> <leader>gw :Gwrite<CR>
            nnoremap <silent> <leader>ge :Gedit<CR>
            " Mnemonic _i_nteractive
            nnoremap <silent> <leader>gi :Git add -p %<CR>
            nnoremap <silent> <leader>gg :SignifyToggle<CR>
        endif
    " }}}

    " OmniComplete {{{
        " if has("autocmd") && exists("+omnifunc")
        "     autocmd Filetype *
        "         \if &omnifunc == "" |
        "         \setlocal omnifunc=syntaxcomplete#Complete |
        "         \endif
        " endif

        hi Pmenu  guifg=#000000 guibg=#F8F8F8 ctermfg=black ctermbg=Lightgray
        hi PmenuSbar  guifg=#8A95A7 guibg=#F8F8F8 gui=NONE ctermfg=darkcyan ctermbg=lightgray cterm=NONE
        hi PmenuThumb  guifg=#F8F8F8 guibg=#8A95A7 gui=NONE ctermfg=lightgray ctermbg=darkcyan cterm=NONE

        " Some convenient mappings
        "inoremap <expr> <Esc>      pumvisible() ? "\<C-e>" : "\<Esc>"
        inoremap <expr> <CR>       pumvisible() ? "\<C-y>" : "\<CR>"
        inoremap <expr> <Down>     pumvisible() ? "\<C-n>" : "\<Down>"
        inoremap <expr> <Up>       pumvisible() ? "\<C-p>" : "\<Up>"
        inoremap <expr> <C-d>      pumvisible() ? "\<PageDown>\<C-p>\<C-n>" : "\<C-d>"
        inoremap <expr> <C-u>      pumvisible() ? "\<PageUp>\<C-p>\<C-n>" : "\<C-u>"

        " Automatically open and close the popup menu / preview window
        " au CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif
        set completeopt-=preview
    " }}}

    " TMUX {{{
        " tmux will send xterm-style keys when its xterm-keys option is on.
        if &term =~ '^screen'
            execute "set <xUp>=\e[1;*A"
            execute "set <xDown>=\e[1;*B"
            execute "set <xRight>=\e[1;*C"
            execute "set <xLeft>=\e[1;*D"
        endif

        " Tmux vim integration
        let g:tmux_navigator_no_mappings = 1
        let g:tmux_navigator_save_on_switch = 1

        " Move between splits with ctrl-w+h,j,k,l
        nnoremap <silent> <c-w>h :TmuxNavigateLeft<cr>
        nnoremap <silent> <c-w>j :TmuxNavigateDown<cr>
        nnoremap <silent> <c-w>k :TmuxNavigateUp<cr>
        nnoremap <silent> <c-w>l :TmuxNavigateRight<cr>
        nnoremap <silent> <c-w>\ :TmuxNavigatePrevious<cr>
    " }}}

    " indent_guides {{{
        if isdirectory(expand(s:rc_path . "/pack/minpac/start/vim-indent-guides/"))
            let g:indent_guides_start_level = 2
            let g:indent_guides_guide_size = 1
            let g:indent_guides_enable_on_vim_startup = 0
        endif
    " }}}

    " Wildfire {{{
        let g:wildfire_objects = {
                \ "*" : ["i'", 'i"', "i)", "i]", "i}", "ip"],
                \ "html,xml" : ["at"],
                \ }
    " }}}

    " vim-airline {{{
        noremap <silent> <Plug>AirlineTablineRefresh :set mod!<CR>
        " Set configuration options for the statusline plugin vim-airline.
        set laststatus=2
        let g:airline_powerline_fonts = 1

        " Enable top tabline.
        let g:airline#extensions#tabline#enabled = 1

        " Disable showing tabs in the tabline. This will ensure that the buffers are
        " what is shown in the tabline at all times.
        let g:airline#extensions#tabline#show_tabs = 0
        let g:airline#extensions#tabline#buffer_nr_show = 1

        " See `:echo g:airline_theme_map` for some more choices
        " Default in terminal vim is 'dark'
        if isdirectory(expand(s:rc_path . "/pack/minpac/start/vim-airline-themes/"))
            if !exists('g:airline_theme')
                let g:airline_theme = 'solarized'
            endif
        endif

        if !exists('g:airline_powerline_fonts')
            " Use the default set of separators with a few customizations
            let g:airline_left_sep='›'  " Slightly fancier than '>'
            let g:airline_right_sep='‹' " Slightly fancier than '<'
        endif

        if !exists('g:airline_symbols')
            let g:airline_symbols = {}
        endif

        " unicode symbols
        let g:airline_left_sep = ''
        let g:airline_right_sep = ''
        " let g:airline_symbols.linenr = '␊'
        " let g:airline_symbols.paste = 'ρ'
        " let g:airline_symbols.whitespace = 'Ξ'
        let g:airline_symbols.branch = ''
        let g:airline_symbols.maxlinenr = ''

        packadd vim-airline
        call airline#parts#define_raw('go', '%#goStatuslineColor#%{go#statusline#Show()}%*%#__restore__#')
        call airline#parts#define_condition('go', '&filetype="go"')
        let g:airline_section_y=airline#section#create_right(['ffenc', 'go'])

    " }}}
" }}}

" Use local vimrc if available {{{
    if filereadable(expand(s:rc_path . "/local.vim"))
        source ~/.config/nvim/local.vim
    endif
" }}}
